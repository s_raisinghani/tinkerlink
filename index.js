const app = require('./api/server').app

const port = 3000

app.listen(port, () => {
  console.log(`API app started on ${port}`)
})

