const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const app = new Koa()

// Redis client
const REDIS_HOST = '127.0.0.1'
const REDIS_PORT = 6379
const redisClient = require('redis').createClient(REDIS_PORT, REDIS_HOST)

// Makes use of async await easier
const asyncRedisClient = require('async-redis').decorate(redisClient)

// Set up Redis Client
asyncRedisClient.on('error', () => {
  console.log('error on client')
})

const videoRouter = require('./videos/router')(asyncRedisClient)

app.use(bodyParser())
app.asyncRedisClient = asyncRedisClient
app
  .use(videoRouter.routes())
  .use(videoRouter.allowedMethods())

module.exports = {
  app,
  redisClient
}
