// Mocking Redis Calls and Database
const videoDb = [];

const redisTimeout = 100

const redisMock = {
  videoDb: videoDb,
  keys: async (search) => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        const keys = videoDb.map((video) => {
          if (video.id.includes(search)) {
            return video.id
          }
        })
        resolve(keys)
      }, redisTimeout);
    })
    return await promise;
  },
  hgetall: async (key) => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        const video = videoDb.find((video) => {
          return video.id == key
        })
        resolve(video)
      }, redisTimeout);
    })
    return await promise;
  },
  HMSET: async (id, payload) => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        videoDb.push(payload)
        resolve(payload)
      }, redisTimeout);
    })
    return await promise;
  }
}

module.exports = redisMock