const supertest = require('supertest')
const server = require('../server')
const expect = require('chai').expect

describe('API Integration Tests', () => {
  let app
  before('Set up API', async () => {
    // Start redis server
    app = server.app.listen(3000)
    request = supertest.agent(app)
  })

  // NOTE: Save one new posting for GET/:id call
  let savedVideo;

  // POST Tests
  describe('POST', () => {
    it('should save video and return entry POST /videos', (done) => {
      const newVideo = {
        description: 'New single video test',
        url: 'http://test.test'
      }

      request.post('/videos').send(newVideo).expect(200).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body.description).to.equal(newVideo.description)
        expect(res.body.id).to.exist
        savedVideo = res.body
        done()
      })
    })

    it('should save multiple videos and return entry POST /videos', (done) => {
      const newVideos = [
        {
          description: 'New multiple video test 1',
          url: 'http://test.test'
        },
        {
          description: 'New multiple video test 2',
          url: 'http://test.test'
        }
      ]

      request.post('/videos').send(newVideos).expect(200).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body[0].description).to.equal(newVideos[0].description)
        expect(res.body[0].url).to.equal(newVideos[0].url)
        expect(res.body[1].id).to.exist
        done()
      })
    })

    it('shouldn\'t proccess empty video POST /videos', (done) => {
      const invalidVideo = null

      request.post('/videos').send(invalidVideo).expect(400).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body).to.be.empty
        done()
      })
    })

    it('shouldn\'t proccess invalid video POST /videos', (done) => {
      const invalidVideo = {
        wrongkey: 'Invalid video'
      }

      request.post('/videos').send(invalidVideo).expect(400).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body).to.be.empty
        done()
      })
    })

    it('shouldn\'t save multiple videos when one is invalid entry POST /videos', (done) => {
      const newVideos = [
        {
          description: 'New multiple video test 1',
          url: 'http://test.test'
        },
        {
          description: 'New multiple video test 2',
          url: 'http://test.test'
        },
        {
          des: 'New invalid multiple video test 3'
        }
      ]

      request.post('/videos').send(newVideos).expect(400).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body).to.be.empty
        done()
      })
    })
  })

  // GET Tests
  describe('GET', () => {
    it('should return an array containing 3 videos GET /videos', (done) => {
      request.get('/videos').expect(200).end((err, res) => {
        if (err) {
          return done(err)
        } 
        expect(res.body).to.be.instanceOf(Array)
        expect(res.body).to.be.length(3)
        done()
      })
    })

    it('should return a single video GET /videos/:id', (done) => {
      request.get(`/videos/${savedVideo.id}`).expect(200).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body.id).to.equal(savedVideo.id.toString())
        expect(res.body.description).to.equal(savedVideo.description)
        expect(res.body.url).to.equal(savedVideo.url)
        done()
      })
    })

    it('should\'t return non-existant video GET /videos/:id', (done) => {
      request.get('/videos/invalid').expect(404).end((err, res) => {
        if (err) {
          return done(err)
        }
        expect(res.body).to.be.empty
        done()
      })
    })
  })

  after('Clean up after tests', async () => {
    server.redisClient.end(true)
    app.close()
  })
})
