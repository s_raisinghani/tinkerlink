const methods = require('../videos/methods')
const expect = require('chai').expect
const redisMock = require('./redisMock')

describe('API Mock Unit Tests', () => {
  describe('savePayload', () => {
    it('should add ID and return video', async () => {
      const video = {
        description: 'test video',
        url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
      }
      const response = await methods.savePayload(redisMock, video)
      expect(response.description).to.equal(video.description)
      expect(response.url).to.equal(video.url)
      expect(response.id).to.equal('videos:0')
    })
  })

  describe('saveArrayPayload', () => {
    it('should add ID and return all videos', async () => {
      const videos = [
        {
          description: 'test video 1',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        },
        {
          description: 'test video 2',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        }
      ]
      const response = await methods.saveArrayPayload(redisMock, videos)
      expect(response[0].description).to.equal(videos[0].description)
      expect(response[0].url).to.equal(videos[0].url)
    })
  })

  describe('getAllKeys', () => {
    it('should return all keys', async () => {
      const response = await methods.getAllKeys(redisMock)
      expect(response.length).to.equal(redisMock.videoDb.length)
    })
  })

  describe('getAllVideos', () => {
    it('should return all videos', async () => {
      const keys = await methods.getAllKeys(redisMock)
      const response = await methods.getAllVideos(redisMock, keys)
      expect(response.length).to.equal(redisMock.videoDb.length)
    })
  })
})
