const helpers = require('../videos/helpers')
const expect = require('chai').expect

describe('Helper Tests', () => {
  describe('checkIfArray', () => {
    it('should return true for valid array', (done) => {
      const array = ['item1', 'item2']
      expect(helpers.checkIfArray(array)).to.be.true
      done()
    })

    it('should return false for empty array', (done) => {
      const array = []
      expect(helpers.checkIfArray(array)).to.be.false
      done()
    })

    it('should return false for invalid array', (done) => {
      const array = 'array'
      expect(helpers.checkIfArray(array)).to.be.false
      done()
    })

    it('should return false for null', (done) => {
      const array = null
      expect(helpers.checkIfArray(array)).to.be.false
      done()
    })
  })

  describe('validateVideo', () => {
    it('should return true for valid video', (done) => {
      const video = {
        description: 'test video',
        url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
      }
      expect(helpers.validateVideo(video)).to.be.true
      done()
    })

    it('should return false for invalid video', (done) => {
      const video = {
        url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
      }
      expect(helpers.validateVideo(video)).to.be.false
      done()
    })

    it('should return false for string', (done) => {
      const video = 'https://www.youtube.com/watch?v=ypuOERjjfKs'
      expect(helpers.validateVideo(video)).to.be.false
      done()
    })
  })

  describe('validateVideoArray', () => {
    it('should return true for valid array', (done) => {
      const videos = [
        {
          description: 'test video 1',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        },
        {
          description: 'test video 2',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        }
      ]
      expect(helpers.validateVideoArray(videos)).to.be.true
      done()
    })

    it('should return false for invalid video in array', (done) => {
      const videos = [
        {
          description: 'test video 1',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        },
        {
          description: 'test video 2',
        }
      ]
      expect(helpers.validateVideoArray(videos)).to.be.false
      done()
    })

    it('should return false for empty object in array', (done) => {
      const videos = [
        {
          description: 'test video 1',
          url: 'https://www.youtube.com/watch?v=ypuOERjjfKs'
        },
        {
        }
      ]
      expect(helpers.validateVideoArray(videos)).to.be.false
      done()
    })
  })

  describe('checkGetParams', () => {
    it('should return true for valid param', (done) => {
      const params = {
        id: 'video:0'
      }
      expect(helpers.checkGetParams(params)).to.be.true
      done()
    })

    it('should return false for invalid param', (done) => {
      const params = {
        test: 'test'
      }
      expect(helpers.checkGetParams(params)).to.be.false
      done()
    })

    it('should return false for string', (done) => {
      const params = 'test'
      expect(helpers.checkGetParams(params)).to.be.false
      done()
    })
  })
})
