// DB Async Functions

// NOTE: Redis isn't really a DB so implementing simple ID
let videoId = 0

const savePayload = async (redis, _payload) => {
  // Add ID
  const payload = {
    id: `videos:${videoId}`,
    ..._payload
  }

  // Add to DB
  const response = await redis.HMSET(payload.id, payload)

  if (!response) {
    return false
  }

  const video = await getVideo(redis, payload.id)
  // Increase ID
  videoId++

  return video
}

const saveArrayPayload = async (redis, _payload) => {
  const payload = []
  for (const video of _payload) {
    payload.push(await savePayload(redis, video))
  }
  return payload
}

const getVideo = async (redis, id) => {
  const video = await redis.hgetall(id)
  return video
}

const getAllKeys = async (redis) => {
  const keys = await redis.keys('videos:*')
  return keys
}

const getAllVideos = async (redis, keys) => {
  const videos = []
  for (const key of keys) {
    const video = await redis.hgetall(key)
    videos.push(video)
  }
  return videos
}

module.exports = {
  savePayload,
  saveArrayPayload,
  getVideo,
  getAllKeys,
  getAllVideos
}
