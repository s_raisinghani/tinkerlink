// Helper Functions
const checkIfArray = (possibleArray) => {
  return Array.isArray(possibleArray) && possibleArray.length > 0
}

const validateVideo = (video) => {
  // TODO: Include some kind of regex for URL
  return video.hasOwnProperty('description') && video.hasOwnProperty('url')
}

const validateVideoArray = (payload) => {
  for (const video of payload) {
    if (!validateVideo(video)) {
      return false
    }
  }
  return true
}

const checkGetParams = (params) => {
  if (params && params.id) {
    return true
  }
  return false
}

const checkIfResponseValid = (response) => {
  // If response is array
  if (checkIfArray(response)) {
    for (const item of response) {
      if (!item) {
        return false
      }
    }
    return true
  }

  // If response is object
  return response !== null && typeof response === 'object'
}

module.exports = {
  checkIfArray,
  validateVideo,
  validateVideoArray,
  checkGetParams,
  checkIfResponseValid
}
