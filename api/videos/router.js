const Router = require('koa-router')
const methods = require('./methods')
const helpers = require('./helpers')

// Set prefix address to /videos
const videoRouter = new Router({ prefix: '/videos' })

module.exports = (redis) => {
  // GET
  videoRouter.get('/', async (ctx) => {
    const keys = await methods.getAllKeys(redis)
    if (helpers.checkIfArray(keys) && keys.length > 0) {
      const videos = await methods.getAllVideos(redis, keys)
      ctx.body = videos
    } else {
      ctx.body = []
    }
  })

  // GET id
  videoRouter.get('/', '/:id', async (ctx) => {
    if (helpers.checkGetParams(ctx.params)) {
      const video = await methods.getVideo(redis, ctx.params.id)
      if (!video) {
        ctx.throw(404, 'Not Found')
      } else {
        ctx.body = video
      }
    } else {
      ctx.throw(400, 'Bad Request')
    }
  })

  // POST
  videoRouter.post('/', async (ctx) => {
    let payload = ctx.request.body

    const isPayloadArray = helpers.checkIfArray(payload)

    const isPayloadValid = isPayloadArray ? helpers.validateVideoArray(payload) : helpers.validateVideo(payload)

    if (isPayloadValid) {
      const response = isPayloadArray ? await methods.saveArrayPayload(redis, payload) : await methods.savePayload(redis, payload)

      // Check if response is valid
      if (helpers.checkIfResponseValid(response)) {
        ctx.body = response
      } else {
        ctx.throw(500, 'Internal Server Error')
      }
    } else {
      ctx.throw(400, 'Bad or Empty Body')
    }
  })

  return videoRouter
}
