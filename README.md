# Node API with Koa + Redis

Test Node API using Koa and Redis

## Getting Started

To run the app:

```
npm run dev
```
**NOTE**: Requires a redis server to be running on port 6379


### Prerequisites

Needs redis installed in the machine


### Installing

To install dependencies:

```
npm install
```

### Try out

Open up Postman on Chrome to carry out manual tests


## Running the tests

To run mock tests:

```
npm run test
```

To run integration tests:

```
npm run test-int
```
**NOTE**: Requires a redis server to be running on port 6379


## Authors

**Sumeet Raisinghani**

## License

ISC